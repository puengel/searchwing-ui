all:
	make build
	make run

build:
	make build-server
	make build-client

build-server:
	cd viewer/server && go build .

build-client:
	cd viewer/web && npm run build-dev

run:
	cd viewer/server && ./server -d ../pics/example_pics/

generate-all:
	make generate-server
	make generate-client

generate-server:
	@protoc -I viewer/server/ --go_out=plugins=grpc:./viewer/server/internal/generated viewer/server/image_service.proto

generate-client:
	@[ -d "viewer/web/src/generated/" ] || mkdir -p ui-gamecut/src/generated
	@protoc -I viewer/server/ --js_out=import_style=commonjs:viewer/web/src/generated --grpc-web_out=import_style=commonjs+dts,mode=grpcwebtext:viewer/web/src/generated  viewer/server/image_service.proto
