package main

import (
	"os"

	"searchwing/viewer/server/internal/application"
	"searchwing/viewer/server/internal/wait"

	"github.com/spf13/cobra"

	"github.com/Benchkram/errz"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

var app *application.App

func init() {

	rootCmd.PersistentFlags().StringP("directory", "d", application.DefaultImageDirectory, "directory to images")

}

var rootCmd = &cobra.Command{
	Use:   "viewservice",
	Short: "viewservice backend",
	Long:  `TODO`,
	Run:   Run,
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		log.Error().Msg(err.Error())
		os.Exit(1)
	}

	log.Debug().Msg("Running: press ctrl+c to stop service")

	wait.ForCtrlC()
	if app != nil {
		err := app.Shutdown()
		if err != nil {
			log.Error().Msg(err.Error())
			os.Exit(1)
		}

	}
}

func Run(cmd *cobra.Command, args []string) {

	// Initial global log config.
	// Will be overwritten by application
	zerolog.TimeFieldFormat = ""
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr}).With().Caller().Logger()

	app = application.New()

	dir := cmd.Flag("directory").Value.String()
	app.Dir = dir

	err := app.Init()
	errz.Fatal(err, "Failed to init application correctly")

	if err := app.Run(); err != nil {
		log.Fatal().Msg(err.Error())
	}
}
