package application

import (
	"net"
	"net/http"
	"os"
	"path/filepath"

	"github.com/Benchkram/errz"
	"github.com/rs/zerolog/log"

	"github.com/rwcarlsen/goexif/exif"
	"github.com/rwcarlsen/goexif/tiff"

	"google.golang.org/grpc"

	pb "searchwing/viewer/server/internal/generated"
)

const (
	DefaultImageDirectory = "."
)

type App struct {
	Dir string

	server *grpc.Server

	Images []ImageData
}

type ImageData struct {
	Path string
	Tiff string
}

type imageMeta struct {
	ImageWidth                       int32
	Make                             string
	ThumbJPEGInterchangeFormatLength int32
	FNumber                          string
	UserComment                      string
	GPSLongitude                     struct {
		hours   string
		minutes string
		seconds string
	}
	ISOSpeedRatings   int32
	ColorSpace        int32
	Model             string
	ExifVersion       string
	DateTimeDigitized string
	ApertureValue     string
	PixelYDimension   int32
	GPSLatitudeRef    string
	DateTime          string
	DateTimeOriginal  string
	ShutterSpeedValue string
	MaxApertureValue  string
	Flash             int32
	FocalLength       string
	GPSLongitudeRef   string
	XResolution       string
	WhiteBalance      int32
	GPSLatitude       struct {
		hours   string
		minutes string
		seconds string
	}
	ExposureTime               string
	MeteringMode               int32
	ImageLength                int32
	YResolution                string
	ResolutionUnit             int32
	YCbCrPositioning           int32
	GPSInfoIFDPointer          int32
	ThumbJPEGInterchangeFormat int32
	MakerNote                  string
	ExposureMode               int32
	InteroperabilityIndex      string
	ExifIFDPointer             int32
	ExposureProgram            int32
	PixelXDimension            int32
	InteroperabilityIFDPointer int32
	ComponentsConfiguration    string
	BrightnessValue            string
	FlashpixVersion            string
	GPSAltitude                string
	GPSImgDirection            string

	// frontendstuff
	requestPath string
}

func New() *App {
	app := &App{}

	return app
}

func (app *App) Init() (err error) {

	var files []string

	// load images
	err = filepath.Walk(app.Dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			log.Err(err)
			return err
		}
		if info.IsDir() {
			return nil
		}
		files = append(files, path)
		return nil
	})

	for _, file := range files {
		// log.Debug().Msgf("file: %s", file)

		// todo get meta info
		f, err := os.Open(file)
		errz.Log(err)

		x, err := exif.Decode(f)
		errz.Log(err)

		// walker := &walker{}
		// x.Walk(walker)

		// log.Debug().Msgf("meta: %v", x)

		meta := x.Tiff.String()

		app.Images = append(app.Images, ImageData{
			Path: file,
			Tiff: meta,
		})

		// imageWidth, err := x.Get("ImageWidth")

		// meta := &pb.ImageMeta{
		// 	ImageWidth: imageWidth,
		// }

		// log.Debug().Msgf("parsed: %v", meta)
	}

	// register grpc server
	app.server = grpc.NewServer()

	pb.RegisterImageServiceServer(app.server, app)

	return
}

type walker struct {
}

func (*walker) Walk(name exif.FieldName, tag *tiff.Tag) error {
	log.Debug().Msgf("%s: %s", name, tag)
	return nil
}

func (app *App) Run() error {

	// host fileserver
	fs := http.FileServer(http.Dir("app"))

	http.Handle("/", fs)

	go http.ListenAndServe(":8080", nil)

	// host metainfo api
	address := ":8081"
	lis, err := net.Listen("tcp", address)
	errz.Fatal(err, "Failed to start server")

	go func() {
		if err := app.server.Serve(lis); err != nil {
			log.Fatal().Msgf("failed to serve: %v", err)
		}
	}()
	log.Info().Msgf("Running grpc server on [%s]", address)

	return nil
}

func (app *App) Shutdown() error {
	app.server.GracefulStop()

	return nil
}
