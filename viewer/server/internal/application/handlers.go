package application

import (
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	pb "searchwing/viewer/server/internal/generated"
)

func (app *App) GetImageMeta(ctx context.Context, req *pb.ImageMetaRequest) (response *pb.ImageMetaResponse, err error) {
	// response = &pb.ImageMetaResponse{
	// 	Images: make([]*pb.ImageMeta, 0),
	// }
	return nil, status.Errorf(codes.Unimplemented, "method GetImageMeta not implemented")
}

func (app *App) GetImagesSimple(ctx context.Context, req *pb.ImageSimpleRequest) (*pb.ImageSimpleResponse, error) {

	images := make([]*pb.ImageSimple, len(app.Images))

	for idx, img := range app.Images {
		images[idx] = &pb.ImageSimple{
			Path: img.Path,
			Tiff: img.Tiff,
		}
	}

	response := &pb.ImageSimpleResponse{
		Images: images,
	}

	return response, nil
}
