import * as React from 'react';
import { FileChooser } from 'domain/filechooser';
import { Route, Switch } from "react-router";
import { View } from 'domain/view/View';


interface RootProps {

}

interface RootState {
}

class Root extends React.Component<RootProps, RootState> {

  constructor(props: RootProps) {
    super(props);
  }

  render() {
    return (

      <Switch >
        <Route path="/" exact
          component={FileChooser}
        />
        <Route path="/view"
          component={View}
        />
      </Switch>
    )
  }
}

const comp = Root;

export { comp as Root }
