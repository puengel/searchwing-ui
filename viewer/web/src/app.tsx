import * as ReactDOM from 'react-dom';
import React from 'react';
import { Root } from "root";
import { BrowserRouter as Router } from 'react-router-dom';



ReactDOM.render(
  <Router>
    <Root />
  </Router>
  ,
  document.getElementById('react-root')
);