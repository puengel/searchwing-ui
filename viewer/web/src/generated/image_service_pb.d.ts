import * as jspb from "google-protobuf"

export class ImageSimple extends jspb.Message {
  getPath(): string;
  setPath(value: string): void;

  getTiff(): string;
  setTiff(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ImageSimple.AsObject;
  static toObject(includeInstance: boolean, msg: ImageSimple): ImageSimple.AsObject;
  static serializeBinaryToWriter(message: ImageSimple, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ImageSimple;
  static deserializeBinaryFromReader(message: ImageSimple, reader: jspb.BinaryReader): ImageSimple;
}

export namespace ImageSimple {
  export type AsObject = {
    path: string,
    tiff: string,
  }
}

export class ImageMeta extends jspb.Message {
  getImagewidth(): number;
  setImagewidth(value: number): void;

  getMake(): string;
  setMake(value: string): void;

  getThumbjpeginterchangeformatlength(): number;
  setThumbjpeginterchangeformatlength(value: number): void;

  getFnumber(): string;
  setFnumber(value: string): void;

  getUsercomment(): string;
  setUsercomment(value: string): void;

  getIsospeedratings(): number;
  setIsospeedratings(value: number): void;

  getColorspace(): number;
  setColorspace(value: number): void;

  getModel(): string;
  setModel(value: string): void;

  getExifversion(): string;
  setExifversion(value: string): void;

  getDatetimedigitized(): string;
  setDatetimedigitized(value: string): void;

  getAperturevalue(): string;
  setAperturevalue(value: string): void;

  getPixelydimension(): number;
  setPixelydimension(value: number): void;

  getGpslatituderef(): string;
  setGpslatituderef(value: string): void;

  getDatetime(): string;
  setDatetime(value: string): void;

  getDatetimeoriginal(): string;
  setDatetimeoriginal(value: string): void;

  getShutterspeedvalue(): string;
  setShutterspeedvalue(value: string): void;

  getMaxaperturevalue(): string;
  setMaxaperturevalue(value: string): void;

  getFlash(): number;
  setFlash(value: number): void;

  getFocallength(): string;
  setFocallength(value: string): void;

  getGpslongituderef(): string;
  setGpslongituderef(value: string): void;

  getXresolution(): string;
  setXresolution(value: string): void;

  getWhitebalance(): number;
  setWhitebalance(value: number): void;

  getExposuretime(): string;
  setExposuretime(value: string): void;

  getMeteringmode(): number;
  setMeteringmode(value: number): void;

  getImagelength(): number;
  setImagelength(value: number): void;

  getYresolution(): string;
  setYresolution(value: string): void;

  getResolutionunit(): number;
  setResolutionunit(value: number): void;

  getYcbcrpositioning(): number;
  setYcbcrpositioning(value: number): void;

  getGpsinfoifdpointer(): number;
  setGpsinfoifdpointer(value: number): void;

  getThumbjpeginterchangeformat(): number;
  setThumbjpeginterchangeformat(value: number): void;

  getMakernote(): string;
  setMakernote(value: string): void;

  getExposuremode(): number;
  setExposuremode(value: number): void;

  getInteroperabilityindex(): string;
  setInteroperabilityindex(value: string): void;

  getExififdpointer(): number;
  setExififdpointer(value: number): void;

  getExposureprogram(): number;
  setExposureprogram(value: number): void;

  getPixelxdimension(): number;
  setPixelxdimension(value: number): void;

  getInteroperabilityifdpointer(): number;
  setInteroperabilityifdpointer(value: number): void;

  getComponentsconfiguration(): string;
  setComponentsconfiguration(value: string): void;

  getBrightnessvalue(): string;
  setBrightnessvalue(value: string): void;

  getFlashpixversion(): string;
  setFlashpixversion(value: string): void;

  getGpsaltitude(): string;
  setGpsaltitude(value: string): void;

  getGpsimgdirection(): string;
  setGpsimgdirection(value: string): void;

  getRequestpath(): string;
  setRequestpath(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ImageMeta.AsObject;
  static toObject(includeInstance: boolean, msg: ImageMeta): ImageMeta.AsObject;
  static serializeBinaryToWriter(message: ImageMeta, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ImageMeta;
  static deserializeBinaryFromReader(message: ImageMeta, reader: jspb.BinaryReader): ImageMeta;
}

export namespace ImageMeta {
  export type AsObject = {
    imagewidth: number,
    make: string,
    thumbjpeginterchangeformatlength: number,
    fnumber: string,
    usercomment: string,
    isospeedratings: number,
    colorspace: number,
    model: string,
    exifversion: string,
    datetimedigitized: string,
    aperturevalue: string,
    pixelydimension: number,
    gpslatituderef: string,
    datetime: string,
    datetimeoriginal: string,
    shutterspeedvalue: string,
    maxaperturevalue: string,
    flash: number,
    focallength: string,
    gpslongituderef: string,
    xresolution: string,
    whitebalance: number,
    exposuretime: string,
    meteringmode: number,
    imagelength: number,
    yresolution: string,
    resolutionunit: number,
    ycbcrpositioning: number,
    gpsinfoifdpointer: number,
    thumbjpeginterchangeformat: number,
    makernote: string,
    exposuremode: number,
    interoperabilityindex: string,
    exififdpointer: number,
    exposureprogram: number,
    pixelxdimension: number,
    interoperabilityifdpointer: number,
    componentsconfiguration: string,
    brightnessvalue: string,
    flashpixversion: string,
    gpsaltitude: string,
    gpsimgdirection: string,
    requestpath: string,
  }

  export class GPSLongitude extends jspb.Message {
    getHours(): string;
    setHours(value: string): void;

    getMinutes(): string;
    setMinutes(value: string): void;

    getSeconds(): string;
    setSeconds(value: string): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): GPSLongitude.AsObject;
    static toObject(includeInstance: boolean, msg: GPSLongitude): GPSLongitude.AsObject;
    static serializeBinaryToWriter(message: GPSLongitude, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): GPSLongitude;
    static deserializeBinaryFromReader(message: GPSLongitude, reader: jspb.BinaryReader): GPSLongitude;
  }

  export namespace GPSLongitude {
    export type AsObject = {
      hours: string,
      minutes: string,
      seconds: string,
    }
  }


  export class GPSLatitude extends jspb.Message {
    getHours(): string;
    setHours(value: string): void;

    getMinutes(): string;
    setMinutes(value: string): void;

    getSeconds(): string;
    setSeconds(value: string): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): GPSLatitude.AsObject;
    static toObject(includeInstance: boolean, msg: GPSLatitude): GPSLatitude.AsObject;
    static serializeBinaryToWriter(message: GPSLatitude, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): GPSLatitude;
    static deserializeBinaryFromReader(message: GPSLatitude, reader: jspb.BinaryReader): GPSLatitude;
  }

  export namespace GPSLatitude {
    export type AsObject = {
      hours: string,
      minutes: string,
      seconds: string,
    }
  }

}

export class ImageMetaRequest extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ImageMetaRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ImageMetaRequest): ImageMetaRequest.AsObject;
  static serializeBinaryToWriter(message: ImageMetaRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ImageMetaRequest;
  static deserializeBinaryFromReader(message: ImageMetaRequest, reader: jspb.BinaryReader): ImageMetaRequest;
}

export namespace ImageMetaRequest {
  export type AsObject = {
  }
}

export class ImageMetaResponse extends jspb.Message {
  getImagesList(): Array<ImageMeta>;
  setImagesList(value: Array<ImageMeta>): void;
  clearImagesList(): void;
  addImages(value?: ImageMeta, index?: number): ImageMeta;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ImageMetaResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ImageMetaResponse): ImageMetaResponse.AsObject;
  static serializeBinaryToWriter(message: ImageMetaResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ImageMetaResponse;
  static deserializeBinaryFromReader(message: ImageMetaResponse, reader: jspb.BinaryReader): ImageMetaResponse;
}

export namespace ImageMetaResponse {
  export type AsObject = {
    imagesList: Array<ImageMeta.AsObject>,
  }
}

export class ImageSimpleRequest extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ImageSimpleRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ImageSimpleRequest): ImageSimpleRequest.AsObject;
  static serializeBinaryToWriter(message: ImageSimpleRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ImageSimpleRequest;
  static deserializeBinaryFromReader(message: ImageSimpleRequest, reader: jspb.BinaryReader): ImageSimpleRequest;
}

export namespace ImageSimpleRequest {
  export type AsObject = {
  }
}

export class ImageSimpleResponse extends jspb.Message {
  getImagesList(): Array<ImageSimple>;
  setImagesList(value: Array<ImageSimple>): void;
  clearImagesList(): void;
  addImages(value?: ImageSimple, index?: number): ImageSimple;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ImageSimpleResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ImageSimpleResponse): ImageSimpleResponse.AsObject;
  static serializeBinaryToWriter(message: ImageSimpleResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ImageSimpleResponse;
  static deserializeBinaryFromReader(message: ImageSimpleResponse, reader: jspb.BinaryReader): ImageSimpleResponse;
}

export namespace ImageSimpleResponse {
  export type AsObject = {
    imagesList: Array<ImageSimple.AsObject>,
  }
}

