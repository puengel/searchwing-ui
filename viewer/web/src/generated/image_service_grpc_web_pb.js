/**
 * @fileoverview gRPC-Web generated client stub for imageservice
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');

const proto = {};
proto.imageservice = require('./image_service_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.imageservice.ImageServiceClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.imageservice.ImageServicePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.imageservice.ImageMetaRequest,
 *   !proto.imageservice.ImageMetaResponse>}
 */
const methodDescriptor_ImageService_GetImageMeta = new grpc.web.MethodDescriptor(
  '/imageservice.ImageService/GetImageMeta',
  grpc.web.MethodType.UNARY,
  proto.imageservice.ImageMetaRequest,
  proto.imageservice.ImageMetaResponse,
  /** @param {!proto.imageservice.ImageMetaRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.imageservice.ImageMetaResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.imageservice.ImageMetaRequest,
 *   !proto.imageservice.ImageMetaResponse>}
 */
const methodInfo_ImageService_GetImageMeta = new grpc.web.AbstractClientBase.MethodInfo(
  proto.imageservice.ImageMetaResponse,
  /** @param {!proto.imageservice.ImageMetaRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.imageservice.ImageMetaResponse.deserializeBinary
);


/**
 * @param {!proto.imageservice.ImageMetaRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.imageservice.ImageMetaResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.imageservice.ImageMetaResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.imageservice.ImageServiceClient.prototype.getImageMeta =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/imageservice.ImageService/GetImageMeta',
      request,
      metadata || {},
      methodDescriptor_ImageService_GetImageMeta,
      callback);
};


/**
 * @param {!proto.imageservice.ImageMetaRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.imageservice.ImageMetaResponse>}
 *     A native promise that resolves to the response
 */
proto.imageservice.ImageServicePromiseClient.prototype.getImageMeta =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/imageservice.ImageService/GetImageMeta',
      request,
      metadata || {},
      methodDescriptor_ImageService_GetImageMeta);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.imageservice.ImageSimpleRequest,
 *   !proto.imageservice.ImageSimpleResponse>}
 */
const methodDescriptor_ImageService_GetImagesSimple = new grpc.web.MethodDescriptor(
  '/imageservice.ImageService/GetImagesSimple',
  grpc.web.MethodType.UNARY,
  proto.imageservice.ImageSimpleRequest,
  proto.imageservice.ImageSimpleResponse,
  /** @param {!proto.imageservice.ImageSimpleRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.imageservice.ImageSimpleResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.imageservice.ImageSimpleRequest,
 *   !proto.imageservice.ImageSimpleResponse>}
 */
const methodInfo_ImageService_GetImagesSimple = new grpc.web.AbstractClientBase.MethodInfo(
  proto.imageservice.ImageSimpleResponse,
  /** @param {!proto.imageservice.ImageSimpleRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.imageservice.ImageSimpleResponse.deserializeBinary
);


/**
 * @param {!proto.imageservice.ImageSimpleRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.imageservice.ImageSimpleResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.imageservice.ImageSimpleResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.imageservice.ImageServiceClient.prototype.getImagesSimple =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/imageservice.ImageService/GetImagesSimple',
      request,
      metadata || {},
      methodDescriptor_ImageService_GetImagesSimple,
      callback);
};


/**
 * @param {!proto.imageservice.ImageSimpleRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.imageservice.ImageSimpleResponse>}
 *     A native promise that resolves to the response
 */
proto.imageservice.ImageServicePromiseClient.prototype.getImagesSimple =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/imageservice.ImageService/GetImagesSimple',
      request,
      metadata || {},
      methodDescriptor_ImageService_GetImagesSimple);
};


module.exports = proto.imageservice;

