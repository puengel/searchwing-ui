import * as grpcWeb from 'grpc-web';

import {
  ImageMetaRequest,
  ImageMetaResponse,
  ImageSimpleRequest,
  ImageSimpleResponse} from './image_service_pb';

export class ImageServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: string; });

  getImageMeta(
    request: ImageMetaRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.Error,
               response: ImageMetaResponse) => void
  ): grpcWeb.ClientReadableStream<ImageMetaResponse>;

  getImagesSimple(
    request: ImageSimpleRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.Error,
               response: ImageSimpleResponse) => void
  ): grpcWeb.ClientReadableStream<ImageSimpleResponse>;

}

export class ImageServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: string; });

  getImageMeta(
    request: ImageMetaRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<ImageMetaResponse>;

  getImagesSimple(
    request: ImageSimpleRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<ImageSimpleResponse>;

}

