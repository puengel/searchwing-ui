import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { ImageServiceClient } from 'generated/image_service_grpc_web_pb';
import { ImageMetaRequest } from 'generated/image_service_pb';

interface ViewProps extends RouteComponentProps {

}

interface ViewState {
  stuff: string;
}

class View extends React.Component<ViewProps, ViewState> {

  constructor(props: ViewProps) {
    super(props);

    this.state = {
      stuff: "",
    }

    this.getStuff()
  }

  private getStuff = async () => {

    let api = new ImageServiceClient("localhost:8081", null, null);

    let req = new ImageMetaRequest();

    api.getImagesSimple(req, {}, (err, response) => {
      if (err) {
        console.log(err);
      }
      let imgs = response.getImagesList();

      let stuff = "";

      for (let i of imgs) {
        stuff = stuff + i.getPath();
      }

    });
  }


  render() {
    const { history } = this.props;
    return (
      <div className="view-container">
        <div className="view-nav-container">
          <button onClick={() => {
            history.push("/");
          }} >home</button>
        </div>
        <div className="view-meta-info" >

        </div>
        <div className="view-main">
          {this.state.stuff}
        </div>
      </div>
    )
  }
}

const comp = withRouter(View);

export { comp as View }
