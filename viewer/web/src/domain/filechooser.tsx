import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';

interface FileChooserProps extends RouteComponentProps {

}

interface FileChooserState {
}

class FileChooser extends React.Component<FileChooserProps, FileChooserState> {

  constructor(props: FileChooserProps) {
   super(props);
  }

  render() {
      const { history } = this.props;
   return (
    <div>
       <h1>Select Folder</h1>
       <input type="file" />
       <button onClick={() => {
           console.log("view");
           history.push("/view");
       }} >view</button>
    </div>
   )
  }
}

const comp = withRouter(FileChooser);

export { comp as FileChooser }
